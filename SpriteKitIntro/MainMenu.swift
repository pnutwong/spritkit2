//
//  MainMenu.swift
//  SpriteKitIntro
//
//  Created by GDD Student on 23/5/16.
//  Copyright © 2016 pnut production. All rights reserved.
//

import SpriteKit


class MainMenu: SKScene {
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let game:GameScene = GameScene(fileNamed: "GameScene")!
        game.scaleMode = .AspectFit
        let transition:SKTransition = SKTransition.crossFadeWithDuration(1.0)
        self.view?.presentScene(game, transition: transition)
    }
}
